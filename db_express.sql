/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 80031 (8.0.31)
 Source Host           : localhost:3306
 Source Schema         : db_express

 Target Server Type    : MySQL
 Target Server Version : 80031 (8.0.31)
 File Encoding         : 65001

 Date: 05/03/2023 14:02:26
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for fast_emps
-- ----------------------------
DROP TABLE IF EXISTS `fast_emps`;
CREATE TABLE `fast_emps` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '记录ID',
  `service_id` int NOT NULL COMMENT '网点ID',
  `job` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '员工职位',
  `uid` int NOT NULL COMMENT '用户id',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `service_id` (`service_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3 ROW_FORMAT=DYNAMIC COMMENT='员工关系表';

-- ----------------------------
-- Records of fast_emps
-- ----------------------------
BEGIN;
INSERT INTO `fast_emps` (`id`, `service_id`, `job`, `uid`) VALUES (1, 1, '24', 4);
COMMIT;

-- ----------------------------
-- Table structure for fast_packs_logs
-- ----------------------------
DROP TABLE IF EXISTS `fast_packs_logs`;
CREATE TABLE `fast_packs_logs` (
  `id` varchar(20) NOT NULL COMMENT '记录ID',
  `send_address` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '寄送地址',
  `send_time` char(19) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '寄送时间',
  `gain_name` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '收货人姓名',
  `gain_address` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '收货人地址',
  `gain_phone` char(11) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '收货人联系电话',
  `collect_time` datetime DEFAULT NULL COMMENT '揽收时间',
  `collect_point` char(13) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '揽收网点',
  `pay_total` double NOT NULL COMMENT '支付费用',
  `status` int NOT NULL DEFAULT '0' COMMENT '快递状态0待接单1待取件2运送中3派件4已完成',
  `send_name` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '寄件人姓名',
  `send_phone` varchar(11) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '寄件人电话',
  `point_id` int DEFAULT NULL COMMENT '网点id',
  `confirm_code` varchar(255) DEFAULT NULL COMMENT '取货码',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 ROW_FORMAT=DYNAMIC COMMENT='包裹';

-- ----------------------------
-- Records of fast_packs_logs
-- ----------------------------
BEGIN;
INSERT INTO `fast_packs_logs` (`id`, `send_address`, `send_time`, `gain_name`, `gain_address`, `gain_phone`, `collect_time`, `collect_point`, `pay_total`, `status`, `send_name`, `send_phone`, `point_id`, `confirm_code`) VALUES ('1', '寄送地址1', '2002-2-2', '收获1', '收获地址1', '12', NULL, NULL, 20, 1, '寄件1', '寄件电话1', 3, '22QW12');
INSERT INTO `fast_packs_logs` (`id`, `send_address`, `send_time`, `gain_name`, `gain_address`, `gain_phone`, `collect_time`, `collect_point`, `pay_total`, `status`, `send_name`, `send_phone`, `point_id`, `confirm_code`) VALUES ('KD1675838385215', '1231', '23123', '1231', '23123', '13123', NULL, NULL, 123123, 4, '123', '12313', 3, '22QW12');
INSERT INTO `fast_packs_logs` (`id`, `send_address`, `send_time`, `gain_name`, `gain_address`, `gain_phone`, `collect_time`, `collect_point`, `pay_total`, `status`, `send_name`, `send_phone`, `point_id`, `confirm_code`) VALUES ('KD1676527767355', '111', NULL, '111', '1111', '12', NULL, NULL, 111, 0, '111', '12', 1, '22QW12');
INSERT INTO `fast_packs_logs` (`id`, `send_address`, `send_time`, `gain_name`, `gain_address`, `gain_phone`, `collect_time`, `collect_point`, `pay_total`, `status`, `send_name`, `send_phone`, `point_id`, `confirm_code`) VALUES ('KD1676535432454', '重庆', NULL, 'hyj3', '湖南', '321123', NULL, NULL, 20, 0, 'hyj', '123321', 3, '22QW12');
INSERT INTO `fast_packs_logs` (`id`, `send_address`, `send_time`, `gain_name`, `gain_address`, `gain_phone`, `collect_time`, `collect_point`, `pay_total`, `status`, `send_name`, `send_phone`, `point_id`, `confirm_code`) VALUES ('KD1676535522883', '四川', NULL, 'hyj3', '浙江', '321123', NULL, NULL, 12, 0, 'hyj', '123321', 5, '22QW12');
INSERT INTO `fast_packs_logs` (`id`, `send_address`, `send_time`, `gain_name`, `gain_address`, `gain_phone`, `collect_time`, `collect_point`, `pay_total`, `status`, `send_name`, `send_phone`, `point_id`, `confirm_code`) VALUES ('KD1676535587599', '北京', NULL, 'hyj3', '新疆', '321123', NULL, NULL, 35, 0, 'hyj', '123321', 11, '22QW12');
INSERT INTO `fast_packs_logs` (`id`, `send_address`, `send_time`, `gain_name`, `gain_address`, `gain_phone`, `collect_time`, `collect_point`, `pay_total`, `status`, `send_name`, `send_phone`, `point_id`, `confirm_code`) VALUES ('KD1676535656920', '山西', NULL, 'hyj3', '江苏', '321123', NULL, NULL, 10, 0, 'hyj', '123321', 10, '22QW12');
INSERT INTO `fast_packs_logs` (`id`, `send_address`, `send_time`, `gain_name`, `gain_address`, `gain_phone`, `collect_time`, `collect_point`, `pay_total`, `status`, `send_name`, `send_phone`, `point_id`, `confirm_code`) VALUES ('KD1676535708902', '湖北', NULL, 'hyj3', '湖南', '321123', NULL, NULL, 50, 0, 'hyj', '123321', 8, '22QW12');
COMMIT;

-- ----------------------------
-- Table structure for fast_send_logs
-- ----------------------------
DROP TABLE IF EXISTS `fast_send_logs`;
CREATE TABLE `fast_send_logs` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '记录ID',
  `emp_id` char(13) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '员工ID',
  `send_time` char(19) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '派送时间',
  `gain_time` char(19) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '揽收时间',
  `pack_id` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '快件ID',
  `service_id` char(13) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '网点ID',
  `status` tinyint DEFAULT '0' COMMENT '派送状态0待接单1待取件2运送中3派件4已完成',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb3 ROW_FORMAT=DYNAMIC COMMENT='派送结果';

-- ----------------------------
-- Records of fast_send_logs
-- ----------------------------
BEGIN;
INSERT INTO `fast_send_logs` (`id`, `emp_id`, `send_time`, `gain_time`, `pack_id`, `service_id`, `status`) VALUES (1, '4', NULL, NULL, 'KD1675838385215', NULL, 1);
COMMIT;

-- ----------------------------
-- Table structure for fast_service_point
-- ----------------------------
DROP TABLE IF EXISTS `fast_service_point`;
CREATE TABLE `fast_service_point` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '记录ID',
  `name` varchar(32) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '网点名称',
  `comm` varchar(125) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '网点描述',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `admin_id` int DEFAULT NULL COMMENT '管理员id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb3 ROW_FORMAT=DYNAMIC COMMENT='网店管理';

-- ----------------------------
-- Records of fast_service_point
-- ----------------------------
BEGIN;
INSERT INTO `fast_service_point` (`id`, `name`, `comm`, `create_time`, `admin_id`) VALUES (1, '网点1', '网点1的描述', '2023-02-06 09:36:19', 3);
INSERT INTO `fast_service_point` (`id`, `name`, `comm`, `create_time`, `admin_id`) VALUES (3, '重庆网点部', '重庆市江北区网点1', '2023-02-16 07:29:25', 3);
INSERT INTO `fast_service_point` (`id`, `name`, `comm`, `create_time`, `admin_id`) VALUES (4, '四川网点部', '四川', '2023-02-16 16:00:39', 14);
INSERT INTO `fast_service_point` (`id`, `name`, `comm`, `create_time`, `admin_id`) VALUES (5, '云南网点部', '云南', '2023-02-16 16:01:10', 15);
INSERT INTO `fast_service_point` (`id`, `name`, `comm`, `create_time`, `admin_id`) VALUES (6, '广东网点部', '广东', '2023-02-16 16:02:07', 16);
INSERT INTO `fast_service_point` (`id`, `name`, `comm`, `create_time`, `admin_id`) VALUES (7, '湖南网点部', '湖南', '2023-02-16 16:02:23', 17);
INSERT INTO `fast_service_point` (`id`, `name`, `comm`, `create_time`, `admin_id`) VALUES (8, '湖北网点部', '湖北', '2023-02-16 16:02:45', 18);
INSERT INTO `fast_service_point` (`id`, `name`, `comm`, `create_time`, `admin_id`) VALUES (9, '河南网点部', '河南', '2023-02-16 16:03:22', 19);
INSERT INTO `fast_service_point` (`id`, `name`, `comm`, `create_time`, `admin_id`) VALUES (10, '山西网点部', '山西', '2023-02-16 16:03:47', 20);
INSERT INTO `fast_service_point` (`id`, `name`, `comm`, `create_time`, `admin_id`) VALUES (11, '北京网点部', '北京', '2023-02-16 16:05:12', 21);
INSERT INTO `fast_service_point` (`id`, `name`, `comm`, `create_time`, `admin_id`) VALUES (12, '山东网点部', NULL, '2023-02-16 16:05:39', 22);
INSERT INTO `fast_service_point` (`id`, `name`, `comm`, `create_time`, `admin_id`) VALUES (13, '浙江网点部', NULL, '2023-02-16 16:06:02', 23);
INSERT INTO `fast_service_point` (`id`, `name`, `comm`, `create_time`, `admin_id`) VALUES (14, '上海网点部', NULL, '2023-02-16 16:07:03', 24);
INSERT INTO `fast_service_point` (`id`, `name`, `comm`, `create_time`, `admin_id`) VALUES (15, '江苏网点部', NULL, '2023-02-16 16:07:33', 25);
INSERT INTO `fast_service_point` (`id`, `name`, `comm`, `create_time`, `admin_id`) VALUES (16, '新疆网点部', NULL, '2023-02-16 16:07:56', 26);
INSERT INTO `fast_service_point` (`id`, `name`, `comm`, `create_time`, `admin_id`) VALUES (17, '福建网点部', NULL, '2023-02-16 16:08:10', 27);
COMMIT;

-- ----------------------------
-- Table structure for fast_transit_logs
-- ----------------------------
DROP TABLE IF EXISTS `fast_transit_logs`;
CREATE TABLE `fast_transit_logs` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '记录ID',
  `send_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '运出时间',
  `send_point` char(13) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '运出网点',
  `next_point` char(13) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '下一站网点',
  `fast_pack_id` char(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '快件ID',
  `emp_id` char(13) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '处理员工ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb3 ROW_FORMAT=DYNAMIC COMMENT='物流信息';

-- ----------------------------
-- Records of fast_transit_logs
-- ----------------------------
BEGIN;
INSERT INTO `fast_transit_logs` (`id`, `send_time`, `send_point`, `next_point`, `fast_pack_id`, `emp_id`) VALUES (2, '2023-02-16 11:52:45', '5', '3', 'KD1676527767355', '3');
INSERT INTO `fast_transit_logs` (`id`, `send_time`, `send_point`, `next_point`, `fast_pack_id`, `emp_id`) VALUES (3, '2023-02-16 11:52:41', '3', '5', 'KD1676527767355', '4');
COMMIT;

-- ----------------------------
-- Table structure for fd_feedback
-- ----------------------------
DROP TABLE IF EXISTS `fd_feedback`;
CREATE TABLE `fd_feedback` (
  `fid` int NOT NULL AUTO_INCREMENT COMMENT '留言id',
  `content` varchar(255) DEFAULT NULL COMMENT '留言内容',
  `uid` int NOT NULL COMMENT '用户id',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`fid`),
  KEY `uid` (`uid`),
  CONSTRAINT `uid` FOREIGN KEY (`uid`) REFERENCES `user` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='留言';

-- ----------------------------
-- Records of fd_feedback
-- ----------------------------
BEGIN;
INSERT INTO `fd_feedback` (`fid`, `content`, `uid`, `create_time`) VALUES (1, '234234234234234', 1, '2023-01-27 12:51:47');
COMMIT;

-- ----------------------------
-- Table structure for fd_recipes
-- ----------------------------
DROP TABLE IF EXISTS `fd_recipes`;
CREATE TABLE `fd_recipes` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '菜谱名称',
  `recipes_type` varchar(20) DEFAULT NULL COMMENT '菜谱类型',
  `description` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '描述',
  `image_link` varchar(500) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '封面图\n',
  `main_ingredients` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '主料',
  `auxiliary_ingredients` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '辅料',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb3 ROW_FORMAT=COMPACT COMMENT='菜谱';

-- ----------------------------
-- Records of fd_recipes
-- ----------------------------
BEGIN;
INSERT INTO `fd_recipes` (`id`, `name`, `recipes_type`, `description`, `image_link`, `main_ingredients`, `auxiliary_ingredients`) VALUES (14, '青椒肉丝', '13', '美食二号', '/2023/01/29/e85eea09-3c57-4100-8236-2285e30582ef.jpg', NULL, NULL);
INSERT INTO `fd_recipes` (`id`, `name`, `recipes_type`, `description`, `image_link`, `main_ingredients`, `auxiliary_ingredients`) VALUES (15, '番茄鸡蛋', '主食', '番茄鸡蛋描述', '/2023/01/29/e85eea09-3c57-4100-8236-2285e30582ef.jpg', '鸡蛋，蛋', '盐，葱花');
INSERT INTO `fd_recipes` (`id`, `name`, `recipes_type`, `description`, `image_link`, `main_ingredients`, `auxiliary_ingredients`) VALUES (16, '223', '主食', '2323', '/2023/01/29/e85eea09-3c57-4100-8236-2285e30582ef.jpg', '323', '2323');
COMMIT;

-- ----------------------------
-- Table structure for menu
-- ----------------------------
DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu` (
  `id` int NOT NULL AUTO_INCREMENT,
  `pattern` varchar(64) DEFAULT NULL,
  `path` varchar(64) DEFAULT NULL,
  `level` int DEFAULT NULL,
  `name` varchar(32) NOT NULL,
  `icon` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT 'el-icon-s-order',
  `parentId` int NOT NULL,
  `enable` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb3 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of menu
-- ----------------------------
BEGIN;
INSERT INTO `menu` (`id`, `pattern`, `path`, `level`, `name`, `icon`, `parentId`, `enable`) VALUES (2, '/', '', 0, '用户管理', 'el-icon-user-solid', 1, 1);
INSERT INTO `menu` (`id`, `pattern`, `path`, `level`, `name`, `icon`, `parentId`, `enable`) VALUES (3, NULL, '/welcome', 1, '首页', 'el-icon-s-order', 1, 1);
INSERT INTO `menu` (`id`, `pattern`, `path`, `level`, `name`, `icon`, `parentId`, `enable`) VALUES (6, '/employee/userList/**', '/userList', 1, '用户列表', 'el-icon-user', 2, 1);
INSERT INTO `menu` (`id`, `pattern`, `path`, `level`, `name`, `icon`, `parentId`, `enable`) VALUES (7, '/staff/roleList/**', '/roleList', 1, '角色列表', 'el-icon-s-check', 2, 1);
INSERT INTO `menu` (`id`, `pattern`, `path`, `level`, `name`, `icon`, `parentId`, `enable`) VALUES (8, '/staff/powList/**', '/powList', 1, '权限列表', 'el-icon-s-tools', 2, 1);
INSERT INTO `menu` (`id`, `pattern`, `path`, `level`, `name`, `icon`, `parentId`, `enable`) VALUES (15, NULL, NULL, 1, '留言管理', 'el-icon-s-order', 1, 1);
INSERT INTO `menu` (`id`, `pattern`, `path`, `level`, `name`, `icon`, `parentId`, `enable`) VALUES (16, NULL, '/feedback', 1, '留言管理', 'el-icon-s-order', 15, 1);
INSERT INTO `menu` (`id`, `pattern`, `path`, `level`, `name`, `icon`, `parentId`, `enable`) VALUES (17, NULL, NULL, 1, '快递模块', 'el-icon-s-order', 1, 1);
INSERT INTO `menu` (`id`, `pattern`, `path`, `level`, `name`, `icon`, `parentId`, `enable`) VALUES (18, NULL, '/express/emps', NULL, '员工管理', 'el-icon-s-order', 17, 0);
INSERT INTO `menu` (`id`, `pattern`, `path`, `level`, `name`, `icon`, `parentId`, `enable`) VALUES (20, NULL, '/express/point', NULL, '网点管理', 'el-icon-s-order', 17, 1);
INSERT INTO `menu` (`id`, `pattern`, `path`, `level`, `name`, `icon`, `parentId`, `enable`) VALUES (21, NULL, '/express/packs', NULL, '寄件管理', 'el-icon-s-order', 17, 1);
INSERT INTO `menu` (`id`, `pattern`, `path`, `level`, `name`, `icon`, `parentId`, `enable`) VALUES (22, NULL, '/express/send', NULL, '派送管理', 'el-icon-s-order', 17, 1);
INSERT INTO `menu` (`id`, `pattern`, `path`, `level`, `name`, `icon`, `parentId`, `enable`) VALUES (23, NULL, '/express/transit', NULL, '物流管理', 'el-icon-s-order', 17, 0);
INSERT INTO `menu` (`id`, `pattern`, `path`, `level`, `name`, `icon`, `parentId`, `enable`) VALUES (24, NULL, '/express/allocation', 1, '快件管理', 'el-icon-s-order', 17, 1);
INSERT INTO `menu` (`id`, `pattern`, `path`, `level`, `name`, `icon`, `parentId`, `enable`) VALUES (25, NULL, '/express/received', 1, '待收货', 'el-icon-s-order', 17, 1);
COMMIT;

-- ----------------------------
-- Table structure for menu_role
-- ----------------------------
DROP TABLE IF EXISTS `menu_role`;
CREATE TABLE `menu_role` (
  `id` int NOT NULL AUTO_INCREMENT,
  `mid` int DEFAULT NULL,
  `rid` int DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `dm` (`mid`) USING BTREE,
  KEY `dr` (`rid`) USING BTREE,
  CONSTRAINT `dm` FOREIGN KEY (`mid`) REFERENCES `menu` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT,
  CONSTRAINT `dr` FOREIGN KEY (`rid`) REFERENCES `role` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE=InnoDB AUTO_INCREMENT=2281 DEFAULT CHARSET=gb2312 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of menu_role
-- ----------------------------
BEGIN;
INSERT INTO `menu_role` (`id`, `mid`, `rid`) VALUES (2240, 15, 17);
INSERT INTO `menu_role` (`id`, `mid`, `rid`) VALUES (2241, 16, 17);
INSERT INTO `menu_role` (`id`, `mid`, `rid`) VALUES (2242, 22, 17);
INSERT INTO `menu_role` (`id`, `mid`, `rid`) VALUES (2243, 17, 17);
INSERT INTO `menu_role` (`id`, `mid`, `rid`) VALUES (2244, 21, 13);
INSERT INTO `menu_role` (`id`, `mid`, `rid`) VALUES (2245, 25, 13);
INSERT INTO `menu_role` (`id`, `mid`, `rid`) VALUES (2246, 15, 13);
INSERT INTO `menu_role` (`id`, `mid`, `rid`) VALUES (2247, 16, 13);
INSERT INTO `menu_role` (`id`, `mid`, `rid`) VALUES (2248, 17, 13);
INSERT INTO `menu_role` (`id`, `mid`, `rid`) VALUES (2262, 23, 16);
INSERT INTO `menu_role` (`id`, `mid`, `rid`) VALUES (2263, 20, 16);
INSERT INTO `menu_role` (`id`, `mid`, `rid`) VALUES (2264, 18, 16);
INSERT INTO `menu_role` (`id`, `mid`, `rid`) VALUES (2265, 24, 16);
INSERT INTO `menu_role` (`id`, `mid`, `rid`) VALUES (2266, 17, 16);
INSERT INTO `menu_role` (`id`, `mid`, `rid`) VALUES (2267, 2, 3);
INSERT INTO `menu_role` (`id`, `mid`, `rid`) VALUES (2268, 8, 3);
INSERT INTO `menu_role` (`id`, `mid`, `rid`) VALUES (2269, 7, 3);
INSERT INTO `menu_role` (`id`, `mid`, `rid`) VALUES (2270, 6, 3);
INSERT INTO `menu_role` (`id`, `mid`, `rid`) VALUES (2271, 23, 3);
INSERT INTO `menu_role` (`id`, `mid`, `rid`) VALUES (2272, 22, 3);
INSERT INTO `menu_role` (`id`, `mid`, `rid`) VALUES (2273, 21, 3);
INSERT INTO `menu_role` (`id`, `mid`, `rid`) VALUES (2274, 20, 3);
INSERT INTO `menu_role` (`id`, `mid`, `rid`) VALUES (2275, 18, 3);
INSERT INTO `menu_role` (`id`, `mid`, `rid`) VALUES (2276, 25, 3);
INSERT INTO `menu_role` (`id`, `mid`, `rid`) VALUES (2277, 15, 3);
INSERT INTO `menu_role` (`id`, `mid`, `rid`) VALUES (2278, 16, 3);
INSERT INTO `menu_role` (`id`, `mid`, `rid`) VALUES (2279, 3, 3);
INSERT INTO `menu_role` (`id`, `mid`, `rid`) VALUES (2280, 17, 3);
COMMIT;

-- ----------------------------
-- Table structure for role
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(32) CHARACTER SET gb2312 COLLATE gb2312_chinese_ci DEFAULT NULL,
  `nameZh` varchar(32) CHARACTER SET gb2312 COLLATE gb2312_chinese_ci DEFAULT NULL,
  `remark` varchar(128) CHARACTER SET gb2312 COLLATE gb2312_chinese_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `nameZh` (`nameZh`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=gb2312 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of role
-- ----------------------------
BEGIN;
INSERT INTO `role` (`id`, `name`, `nameZh`, `remark`) VALUES (3, 'ROLE_warehouse', '超级管理员', '对网站进行后台管理');
INSERT INTO `role` (`id`, `name`, `nameZh`, `remark`) VALUES (13, 'ROLE_aaa', '普通用户', '普通用户');
INSERT INTO `role` (`id`, `name`, `nameZh`, `remark`) VALUES (16, 'ROLE_调度管理员', '网点管理员', '网点管理员');
INSERT INTO `role` (`id`, `name`, `nameZh`, `remark`) VALUES (17, 'ROLE_配送员', '配送员', '配送员');
COMMIT;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT 'hrID',
  `name` varchar(32) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '姓名',
  `phone` char(11) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL COMMENT '手机号码',
  `email` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '邮箱',
  `roleName` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '角色名称',
  `enabled` tinyint(1) DEFAULT '1',
  `username` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '用户名',
  `password` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '密码',
  `userface` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  `remark` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  `sex` varchar(32) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '性别',
  `age` int DEFAULT NULL COMMENT '年龄',
  `tel` int DEFAULT NULL COMMENT '电话',
  `address` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '地址',
  `status` varchar(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL COMMENT '身份',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `phone` (`phone`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8mb3 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of user
-- ----------------------------
BEGIN;
INSERT INTO `user` (`id`, `name`, `phone`, `email`, `roleName`, `enabled`, `username`, `password`, `userface`, `remark`, `sex`, `age`, `tel`, `address`, `status`) VALUES (1, '234', '14616788991', 'pqz@163.com', '超级管理员', 0, 'admin', 'a1bb09ad5dea12e0f94762cb116c447e80c784d8aa2c6625263f7f3436cdd583', 'http://localhost:8181/uploadFile/2020/04/29/22543b71-a7ff-4571-b0a5-65e630e14c1e.JPG', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `user` (`id`, `name`, `phone`, `email`, `roleName`, `enabled`, `username`, `password`, `userface`, `remark`, `sex`, `age`, `tel`, `address`, `status`) VALUES (2, '2353', '12', NULL, '普通用户', 1, 'user1', 'a1bb09ad5dea12e0f94762cb116c447e80c784d8aa2c6625263f7f3436cdd583', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `user` (`id`, `name`, `phone`, `email`, `roleName`, `enabled`, `username`, `password`, `userface`, `remark`, `sex`, `age`, `tel`, `address`, `status`) VALUES (3, '2353', '212', NULL, '网点管理员', 1, 'admin1', 'a1bb09ad5dea12e0f94762cb116c447e80c784d8aa2c6625263f7f3436cdd583', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `user` (`id`, `name`, `phone`, `email`, `roleName`, `enabled`, `username`, `password`, `userface`, `remark`, `sex`, `age`, `tel`, `address`, `status`) VALUES (4, '2353', '343', NULL, '配送员', 1, 'dispatcher', 'a1bb09ad5dea12e0f94762cb116c447e80c784d8aa2c6625263f7f3436cdd583', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `user` (`id`, `name`, `phone`, `email`, `roleName`, `enabled`, `username`, `password`, `userface`, `remark`, `sex`, `age`, `tel`, `address`, `status`) VALUES (8, '22', '22', '222', '配送员', 1, '222', '58ce9b15aee4b33fc824078ab6d6d088f4795a06c675e53bfdc4df93ffb27b01', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `user` (`id`, `name`, `phone`, `email`, `roleName`, `enabled`, `username`, `password`, `userface`, `remark`, `sex`, `age`, `tel`, `address`, `status`) VALUES (9, '33', '33', '333', '配送员', 1, '333', '88c0c2dbf9908d06da214405a49c324f469cc8190ed2a7e44dc75982ce1a6051', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `user` (`id`, `name`, `phone`, `email`, `roleName`, `enabled`, `username`, `password`, `userface`, `remark`, `sex`, `age`, `tel`, `address`, `status`) VALUES (11, '22', '222', '22', '配送员', 1, '2222', '58ce9b15aee4b33fc824078ab6d6d088f4795a06c675e53bfdc4df93ffb27b01', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `user` (`id`, `name`, `phone`, `email`, `roleName`, `enabled`, `username`, `password`, `userface`, `remark`, `sex`, `age`, `tel`, `address`, `status`) VALUES (13, '2222', '2222', '2222', '配送员', 1, '22222', '4d4d7efb3bc23070344511dbd1bfdaa35912bdec0555dcc68eb9f42af237a6d8', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `user` (`id`, `name`, `phone`, `email`, `roleName`, `enabled`, `username`, `password`, `userface`, `remark`, `sex`, `age`, `tel`, `address`, `status`) VALUES (14, 'admin2', '123', 'aaa', '网点管理员', 1, 'admin2', 'a1bb09ad5dea12e0f94762cb116c447e80c784d8aa2c6625263f7f3436cdd583', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `user` (`id`, `name`, `phone`, `email`, `roleName`, `enabled`, `username`, `password`, `userface`, `remark`, `sex`, `age`, `tel`, `address`, `status`) VALUES (15, 'admin3', '123123', 'bbb', '网点管理员', 1, 'admin3', 'a1bb09ad5dea12e0f94762cb116c447e80c784d8aa2c6625263f7f3436cdd583', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `user` (`id`, `name`, `phone`, `email`, `roleName`, `enabled`, `username`, `password`, `userface`, `remark`, `sex`, `age`, `tel`, `address`, `status`) VALUES (16, 'admin4', '124435436', 'hhh', '网点管理员', 1, 'admin4', 'a1bb09ad5dea12e0f94762cb116c447e80c784d8aa2c6625263f7f3436cdd583', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `user` (`id`, `name`, `phone`, `email`, `roleName`, `enabled`, `username`, `password`, `userface`, `remark`, `sex`, `age`, `tel`, `address`, `status`) VALUES (17, 'admin5', '74554', 'uuu', '网点管理员', 1, 'admin5', 'a1bb09ad5dea12e0f94762cb116c447e80c784d8aa2c6625263f7f3436cdd583', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `user` (`id`, `name`, `phone`, `email`, `roleName`, `enabled`, `username`, `password`, `userface`, `remark`, `sex`, `age`, `tel`, `address`, `status`) VALUES (18, 'admin6', '45621', 'ooo', '网点管理员', 1, 'admin6', 'a1bb09ad5dea12e0f94762cb116c447e80c784d8aa2c6625263f7f3436cdd583', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `user` (`id`, `name`, `phone`, `email`, `roleName`, `enabled`, `username`, `password`, `userface`, `remark`, `sex`, `age`, `tel`, `address`, `status`) VALUES (19, 'admin7', '65323', 'kkk', '网点管理员', 1, 'admin7', 'a1bb09ad5dea12e0f94762cb116c447e80c784d8aa2c6625263f7f3436cdd583', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `user` (`id`, `name`, `phone`, `email`, `roleName`, `enabled`, `username`, `password`, `userface`, `remark`, `sex`, `age`, `tel`, `address`, `status`) VALUES (20, 'admin8', '12456', 'yyy', '网点管理员', 1, 'admin8', 'a1bb09ad5dea12e0f94762cb116c447e80c784d8aa2c6625263f7f3436cdd583', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `user` (`id`, `name`, `phone`, `email`, `roleName`, `enabled`, `username`, `password`, `userface`, `remark`, `sex`, `age`, `tel`, `address`, `status`) VALUES (21, 'admin9', '85623', 'ggg', '网点管理员', 1, 'admin9', 'a1bb09ad5dea12e0f94762cb116c447e80c784d8aa2c6625263f7f3436cdd583', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `user` (`id`, `name`, `phone`, `email`, `roleName`, `enabled`, `username`, `password`, `userface`, `remark`, `sex`, `age`, `tel`, `address`, `status`) VALUES (22, 'admin10', '489321', 'iii', '网点管理员', 1, 'admin10', 'a1bb09ad5dea12e0f94762cb116c447e80c784d8aa2c6625263f7f3436cdd583', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `user` (`id`, `name`, `phone`, `email`, `roleName`, `enabled`, `username`, `password`, `userface`, `remark`, `sex`, `age`, `tel`, `address`, `status`) VALUES (23, 'admin11', '1363', 'rrr', '网点管理员', 1, 'admin11', 'a1bb09ad5dea12e0f94762cb116c447e80c784d8aa2c6625263f7f3436cdd583', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `user` (`id`, `name`, `phone`, `email`, `roleName`, `enabled`, `username`, `password`, `userface`, `remark`, `sex`, `age`, `tel`, `address`, `status`) VALUES (24, 'admin12', '15674', 'wee', '网点管理员', 1, 'admin12', 'a1bb09ad5dea12e0f94762cb116c447e80c784d8aa2c6625263f7f3436cdd583', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `user` (`id`, `name`, `phone`, `email`, `roleName`, `enabled`, `username`, `password`, `userface`, `remark`, `sex`, `age`, `tel`, `address`, `status`) VALUES (25, 'admin13', '895656', 'ppp', '网点管理员', 1, 'admin13', 'a1bb09ad5dea12e0f94762cb116c447e80c784d8aa2c6625263f7f3436cdd583', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `user` (`id`, `name`, `phone`, `email`, `roleName`, `enabled`, `username`, `password`, `userface`, `remark`, `sex`, `age`, `tel`, `address`, `status`) VALUES (26, 'admin14', '67345', 'qqw', '网点管理员', 1, 'admin14', 'a1bb09ad5dea12e0f94762cb116c447e80c784d8aa2c6625263f7f3436cdd583', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `user` (`id`, `name`, `phone`, `email`, `roleName`, `enabled`, `username`, `password`, `userface`, `remark`, `sex`, `age`, `tel`, `address`, `status`) VALUES (27, 'admin15', '593200', 'ccc', '网点管理员', 1, 'admin15', 'a1bb09ad5dea12e0f94762cb116c447e80c784d8aa2c6625263f7f3436cdd583', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `user` (`id`, `name`, `phone`, `email`, `roleName`, `enabled`, `username`, `password`, `userface`, `remark`, `sex`, `age`, `tel`, `address`, `status`) VALUES (28, 'huangyujie', '123321', 'ttt', '普通用户', 1, 'hyj', 'a1bb09ad5dea12e0f94762cb116c447e80c784d8aa2c6625263f7f3436cdd583', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `user` (`id`, `name`, `phone`, `email`, `roleName`, `enabled`, `username`, `password`, `userface`, `remark`, `sex`, `age`, `tel`, `address`, `status`) VALUES (29, 'hyj33', '321123', 'cdd', '普通用户', 1, 'hyj3', 'a1bb09ad5dea12e0f94762cb116c447e80c784d8aa2c6625263f7f3436cdd583', NULL, NULL, NULL, NULL, NULL, NULL, NULL);
COMMIT;

-- ----------------------------
-- Table structure for user_role
-- ----------------------------
DROP TABLE IF EXISTS `user_role`;
CREATE TABLE `user_role` (
  `id` int NOT NULL AUTO_INCREMENT,
  `uid` int DEFAULT NULL,
  `rid` int DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `deu` (`uid`) USING BTREE,
  KEY `der` (`rid`) USING BTREE,
  CONSTRAINT `der` FOREIGN KEY (`rid`) REFERENCES `role` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT,
  CONSTRAINT `deu` FOREIGN KEY (`uid`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE=InnoDB AUTO_INCREMENT=104 DEFAULT CHARSET=gb2312 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of user_role
-- ----------------------------
BEGIN;
INSERT INTO `user_role` (`id`, `uid`, `rid`) VALUES (50, 1, 3);
INSERT INTO `user_role` (`id`, `uid`, `rid`) VALUES (71, 2, 13);
INSERT INTO `user_role` (`id`, `uid`, `rid`) VALUES (77, 4, 17);
INSERT INTO `user_role` (`id`, `uid`, `rid`) VALUES (81, 8, 17);
INSERT INTO `user_role` (`id`, `uid`, `rid`) VALUES (82, 9, 17);
INSERT INTO `user_role` (`id`, `uid`, `rid`) VALUES (83, 11, 17);
INSERT INTO `user_role` (`id`, `uid`, `rid`) VALUES (84, 13, 17);
INSERT INTO `user_role` (`id`, `uid`, `rid`) VALUES (87, 3, 16);
INSERT INTO `user_role` (`id`, `uid`, `rid`) VALUES (88, 14, 16);
INSERT INTO `user_role` (`id`, `uid`, `rid`) VALUES (89, 15, 16);
INSERT INTO `user_role` (`id`, `uid`, `rid`) VALUES (90, 16, 16);
INSERT INTO `user_role` (`id`, `uid`, `rid`) VALUES (91, 17, 16);
INSERT INTO `user_role` (`id`, `uid`, `rid`) VALUES (92, 18, 16);
INSERT INTO `user_role` (`id`, `uid`, `rid`) VALUES (93, 19, 16);
INSERT INTO `user_role` (`id`, `uid`, `rid`) VALUES (94, 20, 16);
INSERT INTO `user_role` (`id`, `uid`, `rid`) VALUES (95, 21, 16);
INSERT INTO `user_role` (`id`, `uid`, `rid`) VALUES (96, 22, 16);
INSERT INTO `user_role` (`id`, `uid`, `rid`) VALUES (97, 23, 16);
INSERT INTO `user_role` (`id`, `uid`, `rid`) VALUES (98, 24, 16);
INSERT INTO `user_role` (`id`, `uid`, `rid`) VALUES (99, 25, 16);
INSERT INTO `user_role` (`id`, `uid`, `rid`) VALUES (100, 26, 16);
INSERT INTO `user_role` (`id`, `uid`, `rid`) VALUES (101, 27, 16);
INSERT INTO `user_role` (`id`, `uid`, `rid`) VALUES (102, 28, 13);
INSERT INTO `user_role` (`id`, `uid`, `rid`) VALUES (103, 29, 13);
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
