## 基于SpringBoot框架的物流配送系统的设计与实现主要功能包括:
> 本选题面向实际应用，可自行扩展业务需求，包括但不限于如上功能需求，采用的相关开发技术成熟，有一定的综合性，工作量适性
- 1、菜单权限的分配;角色的管理，不同角色拥有不同的菜单权限
- 2、商家:针对商家可以设置不同的配送计价方案
- 3、配送员管理:针对配送员，系统后来可以清楚地查看配送员的工作情况，查看配送员的评分状况
- 4、调度:根据配送员的订单数量、持单数量等因素，自动挑选合适地配送员，指定订单配送
- 5、以及分货、配货功能、装车、送货功能
