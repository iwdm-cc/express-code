package com.express.boot.domain;

import lombok.Data;

@Data
public class EditRoleTo {
    Integer uid;
    Integer rid;
}
