package com.express.boot.domain;

import lombok.Data;

@Data
public class Menu_role {

    private Integer id;
    private Integer rid;
    private Integer mid;
}
