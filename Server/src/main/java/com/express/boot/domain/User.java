package com.express.boot.domain;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import lombok.ToString;

import java.util.List;

@Data
@ToString
public class User  {

    private Integer id;
    private String name;
    private String phone;
    private String email;
    private String roleName;
    private boolean enabled;
    private String username;
    private String password;
    private String remark;
    /**
     * 盐值
     */
    @TableField(exist = false)
    private String salt = "RvP3UID2n30Q2sycZYvH";
    @TableField(exist = false)
    private List<Role> roles;
    private String userface;

    public void setUsername(String username) {
        this.username = username;
    }

    //    @JsonIgnore
    public String getPassword() {
        return password;
    }

    public String getUsername() {
        return username;
    }

}
