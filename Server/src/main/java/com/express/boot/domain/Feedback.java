package com.express.boot.domain;

import com.express.boot.utils.BaseEntity;
import com.express.boot.utils.annotation.Excel;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 留言对象 fd_feedback
 *
 * @author author__
 * @date 2023-01-27
 */
@Data
public class Feedback extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 留言id
     */
    private Long fid;

    /**
     * 留言内容
     */
    @Excel(name = "留言内容")
    private String content;

    /**
     * 用户id
     */
    @Excel(name = "用户id")
    private Long uid;
    @Excel(name = "用户id")
    private String username;

    public void setFid(Long fid) {
        this.fid = fid;
    }

    public Long getFid() {
        return fid;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    public void setUid(Long uid) {
        this.uid = uid;
    }

    public Long getUid() {
        return uid;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("fid", getFid())
                .append("content", getContent())
                .append("uid", getUid())
                .append("createTime", getCreateTime())
                .toString();
    }
}
