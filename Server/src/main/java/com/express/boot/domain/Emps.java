package com.express.boot.domain;

import com.express.boot.utils.BaseEntity;
import com.express.boot.utils.annotation.Excel;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;


/**
 * 员工关系对象 fast_emps
 *
 * @author author__
 * @date 2023-02-03
 */
@Data
public class Emps extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 记录ID
     */
    private String id;

    /**
     * 网点ID
     */
    @Excel(name = "网点ID")
    private String serviceId;

    /**
     * 员工职位
     */
    @Excel(name = "员工职位")
    private String job;

    @Excel(name = "员工职位")
    private String uid;

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public String getJob() {
        return job;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("serviceId", getServiceId())
                .append("job", getJob())
                .toString();
    }
}
