package com.express.boot.domain;

import com.express.boot.utils.BaseEntity;
import com.express.boot.utils.annotation.Excel;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;



/**
 * 用户对象 fast_users
 * 
 * @author author__
 * @date 2023-02-03
 */
public class Users extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 记录ID */
    private String id;

    /** 用户账号 */
    @Excel(name = "用户账号")
    private String userName;

    /** 用户密码 */
    @Excel(name = "用户密码")
    private String passWord;

    /** 员工姓名 */
    @Excel(name = "员工姓名")
    private String name;

    /** 员工性别 */
    @Excel(name = "员工性别")
    private String gender;

    /** 员工学历 */
    @Excel(name = "员工学历")
    private String record;

    /** 员工年龄 */
    @Excel(name = "员工年龄")
    private Long age;

    /** 用户身份 */
    @Excel(name = "用户身份")
    private Long type;

    public void setId(String id) 
    {
        this.id = id;
    }

    public String getId() 
    {
        return id;
    }
    public void setUserName(String userName) 
    {
        this.userName = userName;
    }

    public String getUserName() 
    {
        return userName;
    }
    public void setPassWord(String passWord) 
    {
        this.passWord = passWord;
    }

    public String getPassWord() 
    {
        return passWord;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setGender(String gender) 
    {
        this.gender = gender;
    }

    public String getGender() 
    {
        return gender;
    }
    public void setRecord(String record) 
    {
        this.record = record;
    }

    public String getRecord() 
    {
        return record;
    }
    public void setAge(Long age) 
    {
        this.age = age;
    }

    public Long getAge() 
    {
        return age;
    }
    public void setType(Long type) 
    {
        this.type = type;
    }

    public Long getType() 
    {
        return type;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("userName", getUserName())
            .append("passWord", getPassWord())
            .append("name", getName())
            .append("gender", getGender())
            .append("record", getRecord())
            .append("age", getAge())
            .append("createTime", getCreateTime())
            .append("type", getType())
            .toString();
    }
}
