package com.express.boot.domain;

import com.express.boot.utils.BaseEntity;
import com.express.boot.utils.annotation.Excel;
import lombok.Data;


/**
 * 网店管理对象 fast_service_point
 *
 * @author author__
 * @date 2023-02-03
 */
@Data
public class ServicePoint extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 记录ID
     */
    private String id;

    /**
     * 网点名称
     */
    @Excel(name = "网点名称")
    private String name;

    /**
     * 网点描述
     */
    @Excel(name = "网点描述")
    private String comm;

    @Excel(name = "管理员id")
    private long adminId;


}
