package com.express.boot.domain;

import com.express.boot.utils.BaseEntity;
import com.express.boot.utils.annotation.Excel;
import lombok.Data;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;


/**
 * 派送结果对象 fast_send_logs
 *
 * @author author__
 * @date 2023-02-03
 */
@Data
public class SendLogs extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 记录ID
     */
    private String id;

    /**
     * 员工ID
     */
    @Excel(name = "员工ID")
    private String empId;

    /**
     * 派送时间
     */
    @Excel(name = "派送时间")
    private String sendTime;

    /**
     * 揽收时间
     */
    @Excel(name = "揽收时间")
    private String gainTime;

    /**
     * 快件ID
     */
    @Excel(name = "快件ID")
    private String packId;

    /**
     * 网点ID
     */
    @Excel(name = "网点ID")
    private String serviceId;

    private long status;

    private String confirmCode;

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("id" , getId())
                .append("empId" , getEmpId())
                .append("sendTime" , getSendTime())
                .append("gainTime" , getGainTime())
                .append("packId" , getPackId())
                .append("serviceId" , getServiceId())
                .toString();
    }
}
