package com.express.boot.domain;

import com.express.boot.utils.BaseEntity;
import com.express.boot.utils.annotation.Excel;
import lombok.Data;


/**
 * 包裹对象 fast_packs_logs
 *
 * @author author__
 * @date 2023-02-03
 */
@Data
public class PacksLogs extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 记录ID
     */
    private String id;

    /**
     * 寄送地址
     */
    @Excel(name = "寄送地址")
    private String sendAddress;

    /**
     * 寄送时间
     */
    @Excel(name = "寄送时间")
    private String sendTime;

    /**
     * 收货人姓名
     */
    @Excel(name = "收货人姓名")
    private String gainName;

    /**
     * 收货人地址
     */
    @Excel(name = "收货人地址")
    private String gainAddress;

    /**
     * 收货人联系电话
     */
    @Excel(name = "收货人联系电话")
    private String gainPhone;

    /**
     * 揽收时间
     */
    @Excel(name = "揽收时间")
    private String collectTime;

    /**
     * 揽收网点
     */
    @Excel(name = "揽收网点")
    private String collectPoint;

    /**
     * 支付费用
     */
    @Excel(name = "支付费用")
    private Long payTotal;

    /**
     * 快递状态
     */
    @Excel(name = "快递状态")
    private Long status;
    @Excel(name = "网点id")
    private Long pointId;

    /**
     * 寄件人姓名
     */
    @Excel(name = "寄件人姓名")
    private String sendName;

    /**
     * 寄件人电话
     */
    @Excel(name = "寄件人电话")
    private String sendPhone;
    @Excel(name = "取货码")

    private String confirmCode;


}
