package com.express.boot.domain;

import com.express.boot.utils.BaseEntity;
import com.express.boot.utils.annotation.Excel;
import lombok.Data;

import java.util.Date;


/**
 * 物流信息对象 fast_transit_logs
 *
 * @author author__
 * @date 2023-02-03
 */
@Data
public class TransitLogs extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 记录ID
     */
    private String id;

    /**
     * 运出时间
     */
    @Excel(name = "运出时间")
    private Date sendTime;

    /**
     * 运出网点
     */
    @Excel(name = "运出网点")
    private String sendPoint;

    /**
     * 下一站网点
     */
    @Excel(name = "下一站网点")
    private String nextPoint;

    /**
     * 快件ID
     */
    @Excel(name = "快件ID")
    private String fastPackId;

    /**
     * 处理员工ID
     */
    @Excel(name = "处理员工ID")
    private String empId;

}
