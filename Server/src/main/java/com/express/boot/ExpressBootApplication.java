package com.express.boot;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableCaching
@EnableTransactionManagement(proxyTargetClass = true)
@MapperScan(basePackages = "com.**.mapper")
public class ExpressBootApplication {
    public static void main(String[] args) {
        SpringApplication.run(ExpressBootApplication.class, args);
    }

}
