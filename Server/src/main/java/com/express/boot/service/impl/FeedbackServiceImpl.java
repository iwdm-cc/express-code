package com.express.boot.service.impl;

import com.express.boot.domain.Feedback;
import com.express.boot.mapper.FeedbackMapper;
import com.express.boot.service.IFeedbackService;
import com.express.boot.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 留言Service业务层处理
 * 
 * @author author__
 * @date 2023-01-27
 */
@Service
public class FeedbackServiceImpl implements IFeedbackService 
{
    @Autowired
    private FeedbackMapper feedbackMapper;

    /**
     * 查询留言
     * 
     * @param fid 留言主键
     * @return 留言
     */
    @Override
    public Feedback selectFeedbackByFid(Long fid)
    {
        return feedbackMapper.selectFeedbackByFid(fid);
    }

    /**
     * 查询留言列表
     * 
     * @param feedback 留言
     * @return 留言
     */
    @Override
    public List<Feedback> selectFeedbackList(Feedback feedback)
    {
        return feedbackMapper.selectFeedbackList(feedback);
    }

    /**
     * 新增留言
     * 
     * @param feedback 留言
     * @return 结果
     */
    @Override
    public int insertFeedback(Feedback feedback)
    {
        feedback.setCreateTime(DateUtils.getNowDate());
        return feedbackMapper.insertFeedback(feedback);
    }

    /**
     * 修改留言
     * 
     * @param feedback 留言
     * @return 结果
     */
    @Override
    public int updateFeedback(Feedback feedback)
    {
        return feedbackMapper.updateFeedback(feedback);
    }

    /**
     * 批量删除留言
     * 
     * @param fids 需要删除的留言主键
     * @return 结果
     */
    @Override
    public int deleteFeedbackByFids(Long[] fids)
    {
        return feedbackMapper.deleteFeedbackByFids(fids);
    }

    /**
     * 删除留言信息
     * 
     * @param fid 留言主键
     * @return 结果
     */
    @Override
    public int deleteFeedbackByFid(Long fid)
    {
        return feedbackMapper.deleteFeedbackByFid(fid);
    }
}
