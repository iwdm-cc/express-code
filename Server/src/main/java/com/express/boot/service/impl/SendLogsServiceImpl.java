package com.express.boot.service.impl;

import com.express.boot.domain.PacksLogs;
import com.express.boot.domain.SendLogs;
import com.express.boot.mapper.PacksLogsMapper;
import com.express.boot.mapper.SendLogsMapper;
import com.express.boot.service.ISendLogsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 派送结果Service业务层处理
 *
 * @author author__
 * @date 2023-02-03
 */
@Service
public class SendLogsServiceImpl implements ISendLogsService {
    @Autowired
    private SendLogsMapper sendLogsMapper;
    @Autowired
    private PacksLogsMapper packsLogsMapper;

    /**
     * 查询派送结果
     *
     * @param id 派送结果主键
     * @return 派送结果
     */
    @Override
    public SendLogs selectSendLogsById(String id) {
        return sendLogsMapper.selectSendLogsById(id);
    }

    /**
     * 查询派送结果列表
     *
     * @param sendLogs 派送结果
     * @return 派送结果
     */
    @Override
    public List<SendLogs> selectSendLogsList(SendLogs sendLogs) {
        return sendLogsMapper.selectSendLogsList(sendLogs);
    }

    /**
     * 新增派送结果
     *
     * @param sendLogs 派送结果
     * @return 结果
     */
    @Override
    public int insertSendLogs(SendLogs sendLogs) {
        return sendLogsMapper.insertSendLogs(sendLogs);
    }

    /**
     * 修改派送结果
     *
     * @param sendLogs 派送结果
     * @return 结果
     */
    @Override
    public int updateSendLogs(SendLogs sendLogs) {

        //派送成功，快递员操作
        //更新 订单状态
        if (sendLogs.getStatus() == 1){
            PacksLogs p= new PacksLogs();
            p.setId(sendLogs.getPackId());
            p.setStatus(4L);
            packsLogsMapper.updatePacksLogs(p);
        }

        return sendLogsMapper.updateSendLogs(sendLogs);
    }

    /**
     * 批量删除派送结果
     *
     * @param ids 需要删除的派送结果主键
     * @return 结果
     */
    @Override
    public int deleteSendLogsByIds(String[] ids) {
        return sendLogsMapper.deleteSendLogsByIds(ids);
    }

    /**
     * 删除派送结果信息
     *
     * @param id 派送结果主键
     * @return 结果
     */
    @Override
    public int deleteSendLogsById(String id) {
        return sendLogsMapper.deleteSendLogsById(id);
    }
}
