package com.express.boot.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.express.boot.domain.User_role;
import com.express.boot.mapper.User_roleMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
@Slf4j
public class User_roleService extends ServiceImpl<User_roleMapper, User_role> {
    @Autowired
    User_roleMapper user_roleMapper;

    public boolean save(User_role entity) {
        int i = user_roleMapper.insert(entity);
        if (i != 0) {
            log.info("分配角色");
            return true;
        } else {
            return false;
        }

    }
}
