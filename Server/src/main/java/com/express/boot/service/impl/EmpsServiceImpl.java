package com.express.boot.service.impl;

import com.express.boot.domain.Emps;
import com.express.boot.mapper.EmpsMapper;
import com.express.boot.service.IEmpsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 员工关系Service业务层处理
 * 
 * @author author__
 * @date 2023-02-03
 */
@Service
public class EmpsServiceImpl implements IEmpsService 
{
    @Autowired
    private EmpsMapper empsMapper;

    /**
     * 查询员工关系
     * 
     * @param id 员工关系主键
     * @return 员工关系
     */
    @Override
    public Emps selectEmpsById(String id)
    {
        return empsMapper.selectEmpsById(id);
    }

    /**
     * 查询员工关系列表
     * 
     * @param emps 员工关系
     * @return 员工关系
     */
    @Override
    public List<Emps> selectEmpsList(Emps emps)
    {
        return empsMapper.selectEmpsList(emps);
    }

    /**
     * 新增员工关系
     * 
     * @param emps 员工关系
     * @return 结果
     */
    @Override
    public int insertEmps(Emps emps)
    {
        return empsMapper.insertEmps(emps);
    }

    /**
     * 修改员工关系
     * 
     * @param emps 员工关系
     * @return 结果
     */
    @Override
    public int updateEmps(Emps emps)
    {
        return empsMapper.updateEmps(emps);
    }

    /**
     * 批量删除员工关系
     * 
     * @param ids 需要删除的员工关系主键
     * @return 结果
     */
    @Override
    public int deleteEmpsByIds(String[] ids)
    {
        return empsMapper.deleteEmpsByIds(ids);
    }

    /**
     * 删除员工关系信息
     * 
     * @param id 员工关系主键
     * @return 结果
     */
    @Override
    public int deleteEmpsById(String id)
    {
        return empsMapper.deleteEmpsById(id);
    }
}
