package com.express.boot.service.impl;

import com.express.boot.domain.PacksLogs;
import com.express.boot.domain.TransitLogs;
import com.express.boot.mapper.PacksLogsMapper;
import com.express.boot.mapper.TransitLogsMapper;
import com.express.boot.service.ITransitLogsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 物流信息Service业务层处理
 *
 * @author author__
 * @date 2023-02-03
 */
@Service
public class TransitLogsServiceImpl implements ITransitLogsService {
    @Autowired
    private TransitLogsMapper transitLogsMapper;
    @Autowired
    private PacksLogsMapper packsLogsMapper;

    /**
     * 查询物流信息
     *
     * @param id 物流信息主键
     * @return 物流信息
     */
    @Override
    public TransitLogs selectTransitLogsById(String id) {
        return transitLogsMapper.selectTransitLogsById(id);
    }

    /**
     * 查询物流信息列表
     *
     * @param transitLogs 物流信息
     * @return 物流信息
     */
    @Override
    public List<TransitLogs> selectTransitLogsList(TransitLogs transitLogs) {
        return transitLogsMapper.selectTransitLogsList(transitLogs);
    }
    public List<TransitLogs> selectTransitLogsVoByPack(TransitLogs transitLogs) {
        return transitLogsMapper.selectTransitLogsVoByPack(transitLogs);
    }

    /**
     * 新增物流信息
     *
     * @param transitLogs 物流信息
     * @return 结果
     */
    @Override
    public int insertTransitLogs(TransitLogs transitLogs) {

        // 更新快递当的 网点
        PacksLogs pack = new PacksLogs();
        pack.setId(transitLogs.getFastPackId());
        pack.setPointId(Long.valueOf(transitLogs.getNextPoint()));
        packsLogsMapper.updatePacksLogs(pack);

        return transitLogsMapper.insertTransitLogs(transitLogs);
    }

    /**
     * 修改物流信息
     *
     * @param transitLogs 物流信息
     * @return 结果
     */
    @Override
    public int updateTransitLogs(TransitLogs transitLogs) {
        return transitLogsMapper.updateTransitLogs(transitLogs);
    }

    /**
     * 批量删除物流信息
     *
     * @param ids 需要删除的物流信息主键
     * @return 结果
     */
    @Override
    public int deleteTransitLogsByIds(String[] ids) {
        return transitLogsMapper.deleteTransitLogsByIds(ids);
    }

    /**
     * 删除物流信息信息
     *
     * @param id 物流信息主键
     * @return 结果
     */
    @Override
    public int deleteTransitLogsById(String id) {
        return transitLogsMapper.deleteTransitLogsById(id);
    }
}
