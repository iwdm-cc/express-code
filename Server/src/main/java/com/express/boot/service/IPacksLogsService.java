package com.express.boot.service;

import com.express.boot.domain.PacksLogs;

import java.util.List;
import java.util.Set;

/**
 * 包裹Service接口
 *
 * @author author__
 * @date 2023-02-03
 */
public interface IPacksLogsService
{
    /**
     * 查询包裹
     *
     * @param id 包裹主键
     * @return 包裹
     */
    public PacksLogs selectPacksLogsById(String id);

    /**
     * 查询包裹列表
     *
     * @param packsLogs 包裹
     * @return 包裹集合
     */
    public List<PacksLogs> selectPacksLogsList(PacksLogs packsLogs);

    /**
     * 新增包裹
     *
     * @param packsLogs 包裹
     * @return 结果
     */
    public int insertPacksLogs(PacksLogs packsLogs);

    /**
     * 修改包裹
     *
     * @param packsLogs 包裹
     * @return 结果
     */
    public int updatePacksLogs(PacksLogs packsLogs);

    /**
     * 批量删除包裹
     *
     * @param ids 需要删除的包裹主键集合
     * @return 结果
     */
    public int deletePacksLogsByIds(String[] ids);

    /**
     * 删除包裹信息
     *
     * @param id 包裹主键
     * @return 结果
     */
    public int deletePacksLogsById(String id);

    List<PacksLogs> selectPacksLogsListBySelf(PacksLogs packsLogs);

    List<PacksLogs> selectPacksLogsListByPoints(Set<String> collect);
}
