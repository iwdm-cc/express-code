package com.express.boot.service.impl;

import com.express.boot.domain.ServicePoint;
import com.express.boot.mapper.ServicePointMapper;
import com.express.boot.service.IServicePointService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 网店管理Service业务层处理
 *
 * @author author__
 * @date 2023-02-03
 */
@Service
public class ServicePointServiceImpl implements IServicePointService {
    @Autowired
    private ServicePointMapper servicePointMapper;

    /**
     * 查询网店管理
     *
     * @param id 网店管理主键
     * @return 网店管理
     */
    @Override
    public ServicePoint selectServicePointById(String id) {
        return servicePointMapper.selectServicePointById(id);
    }

    /**
     * 查询网店管理列表
     *
     * @param servicePoint 网店管理
     * @return 网店管理
     */
    @Override
    public List<ServicePoint> selectServicePointList(ServicePoint servicePoint) {
        return servicePointMapper.selectServicePointList(servicePoint);
    }

    /**
     * 新增网店管理
     *
     * @param servicePoint 网店管理
     * @return 结果
     */
    @Override
    public int insertServicePoint(ServicePoint servicePoint) {
//        servicePoint.setCreateTime(DateUtils.getNowDate());
        return servicePointMapper.insertServicePoint(servicePoint);
    }

    /**
     * 修改网店管理
     *
     * @param servicePoint 网店管理
     * @return 结果
     */
    @Override
    public int updateServicePoint(ServicePoint servicePoint) {
        return servicePointMapper.updateServicePoint(servicePoint);
    }

    /**
     * 批量删除网店管理
     *
     * @param ids 需要删除的网店管理主键
     * @return 结果
     */
    @Override
    public int deleteServicePointByIds(String[] ids) {
        return servicePointMapper.deleteServicePointByIds(ids);
    }

    /**
     * 删除网店管理信息
     *
     * @param id 网店管理主键
     * @return 结果
     */
    @Override
    public int deleteServicePointById(String id) {
        return servicePointMapper.deleteServicePointById(id);
    }
}
