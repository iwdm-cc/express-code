package com.express.boot.service;

import com.express.boot.domain.ServicePoint;

import java.util.List;

/**
 * 网店管理Service接口
 * 
 * @author author__
 * @date 2023-02-03
 */
public interface IServicePointService 
{
    /**
     * 查询网店管理
     * 
     * @param id 网店管理主键
     * @return 网店管理
     */
    public ServicePoint selectServicePointById(String id);

    /**
     * 查询网店管理列表
     * 
     * @param servicePoint 网店管理
     * @return 网店管理集合
     */
    public List<ServicePoint> selectServicePointList(ServicePoint servicePoint);

    /**
     * 新增网店管理
     * 
     * @param servicePoint 网店管理
     * @return 结果
     */
    public int insertServicePoint(ServicePoint servicePoint);

    /**
     * 修改网店管理
     * 
     * @param servicePoint 网店管理
     * @return 结果
     */
    public int updateServicePoint(ServicePoint servicePoint);

    /**
     * 批量删除网店管理
     * 
     * @param ids 需要删除的网店管理主键集合
     * @return 结果
     */
    public int deleteServicePointByIds(String[] ids);

    /**
     * 删除网店管理信息
     * 
     * @param id 网店管理主键
     * @return 结果
     */
    public int deleteServicePointById(String id);
}
