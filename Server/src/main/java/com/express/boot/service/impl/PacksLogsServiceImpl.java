package com.express.boot.service.impl;

import com.express.boot.domain.PacksLogs;
import com.express.boot.mapper.PacksLogsMapper;
import com.express.boot.service.IPacksLogsService;
import com.express.boot.utils.RandomValidateCodeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

/**
 * 包裹Service业务层处理
 *
 * @author author__
 * @date 2023-02-03
 */
@Service
public class PacksLogsServiceImpl implements IPacksLogsService {
    @Autowired
    private PacksLogsMapper packsLogsMapper;

    /**
     * 查询包裹
     *
     * @param id 包裹主键
     * @return 包裹
     */
    @Override
    public PacksLogs selectPacksLogsById(String id) {
        return packsLogsMapper.selectPacksLogsById(id);
    }

    /**
     * 查询包裹列表
     *
     * @param packsLogs 包裹
     * @return 包裹
     */
    @Override
    public List<PacksLogs> selectPacksLogsList(PacksLogs packsLogs) {
        return packsLogsMapper.selectPacksLogsList(packsLogs);
    }

    /**
     * 新增包裹
     *
     * @param packsLogs 包裹
     * @return 结果
     */
    @Override
    public int insertPacksLogs(PacksLogs packsLogs) {
        packsLogs.setConfirmCode(RandomValidateCodeUtil.generateVerifyCode(6));
        packsLogs.setId("KD" + System.currentTimeMillis());
        return packsLogsMapper.insertPacksLogs(packsLogs);
    }

    /**
     * 修改包裹
     *
     * @param packsLogs 包裹
     * @return 结果
     */
    @Override
    public int updatePacksLogs(PacksLogs packsLogs) {
        return packsLogsMapper.updatePacksLogs(packsLogs);
    }

    /**
     * 批量删除包裹
     *
     * @param ids 需要删除的包裹主键
     * @return 结果
     */
    @Override
    public int deletePacksLogsByIds(String[] ids) {
        return packsLogsMapper.deletePacksLogsByIds(ids);
    }

    /**
     * 删除包裹信息
     *
     * @param id 包裹主键
     * @return 结果
     */
    @Override
    public int deletePacksLogsById(String id) {
        return packsLogsMapper.deletePacksLogsById(id);
    }

    @Override
    public List<PacksLogs> selectPacksLogsListBySelf(PacksLogs packsLogs) {
        return packsLogsMapper.selectPacksLogsListBySelf(packsLogs);
    }

    @Override
    public List<PacksLogs> selectPacksLogsListByPoints(Set<String> collect) {
        return packsLogsMapper.selectPacksLogsListByPoints(collect);
    }
}
