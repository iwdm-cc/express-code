package com.express.boot.controller;

import com.express.boot.config.security.ShiroUtils;
import com.express.boot.domain.Feedback;
import com.express.boot.domain.User;
import com.express.boot.service.IFeedbackService;
import com.express.boot.utils.AjaxResult;
import com.express.boot.utils.BaseController;
import com.express.boot.utils.page.TableDataInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 留言Controller
 *
 * @author author__
 * @date 2023-01-27
 */
@RestController
@RequestMapping("/boot/feedback")
public class FeedbackController extends BaseController {
    @Autowired
    private IFeedbackService feedbackService;

    /**
     * 查询留言列表
     */

    @GetMapping("/list")
    public TableDataInfo list(Feedback feedback) {
        User userInfo = ShiroUtils.getUserInfo();
        if (userInfo != null) {
            if (userInfo.getId() != 1)
                feedback.setUid(Long.valueOf(userInfo.getId()));
        }
        startPage();
        List<Feedback> list = feedbackService.selectFeedbackList(feedback);
        return getDataTable(list);
    }


    /**
     * 获取留言详细信息
     */

    @GetMapping(value = "/{fid}")
    public AjaxResult getInfo(@PathVariable("fid") Long fid) {
        return AjaxResult.success(feedbackService.selectFeedbackByFid(fid));
    }

    /**
     * 新增留言
     */

    @PostMapping
    public AjaxResult add(@RequestBody Feedback feedback) {
        User userInfo = ShiroUtils.getUserInfo();
        feedback.setUid(Long.valueOf(userInfo.getId()));
        return toAjax(feedbackService.insertFeedback(feedback));
    }

    /**
     * 修改留言
     */


    @PutMapping
    public AjaxResult edit(@RequestBody Feedback feedback) {
        return toAjax(feedbackService.updateFeedback(feedback));
    }

    /**
     * 删除留言
     */

    @DeleteMapping("/{fids}")
    public AjaxResult remove(@PathVariable Long[] fids) {
        return toAjax(feedbackService.deleteFeedbackByFids(fids));
    }
}
