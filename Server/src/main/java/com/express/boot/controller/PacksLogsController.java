package com.express.boot.controller;

import com.express.boot.domain.PacksLogs;
import com.express.boot.domain.ServicePoint;
import com.express.boot.domain.User;
import com.express.boot.service.IPacksLogsService;
import com.express.boot.service.IServicePointService;
import com.express.boot.utils.AjaxResult;
import com.express.boot.utils.BaseController;
import com.express.boot.utils.ExcelUtil;
import com.express.boot.utils.UserUtil;
import com.express.boot.utils.page.TableDataInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


/**
 * 包裹Controller
 *
 * @author author__
 * @date 2023-02-03
 */
@RestController
@RequestMapping("/boot/packs")
public class PacksLogsController extends BaseController {
    @Autowired
    private IPacksLogsService packsLogsService;
    @Autowired
    private IServicePointService servicePointService;

    /**
     * 查询包裹列表
     */

    @GetMapping("/list")
    public TableDataInfo list(PacksLogs packsLogs) {
        User currentUser = UserUtil.getCurrentUser();
        System.out.println(currentUser);
        startPage();
        List<PacksLogs> list;
        if ("普通用户".equals(currentUser.getRoleName()) && !currentUser.getPhone().isEmpty()) {
            packsLogs.setSendPhone(currentUser.getPhone());
            list = packsLogsService.selectPacksLogsListBySelf(packsLogs);
        } else {
            list = packsLogsService.selectPacksLogsList(packsLogs);
        }

        return getDataTable(list);
    }

    /**
     * 网点管理员 获取该网点的订单
     */

    @GetMapping("/allocation")
    public TableDataInfo allocation(PacksLogs packsLogs) {
        User currentUser = UserUtil.getCurrentUser();
        System.out.println(currentUser);
        ServicePoint po = new ServicePoint();
        po.setAdminId(currentUser.getId());
        Set<String> collect = servicePointService.selectServicePointList(po).stream().map(ServicePoint::getId).collect(Collectors.toSet());

        startPage();
        List<PacksLogs> list;
        if (collect.size() > 0)
            list = packsLogsService.selectPacksLogsListByPoints(collect);
        else
            list = new ArrayList<>();

        return getDataTable(list);
    }

    /**
     * 查询 待收货 包裹列表
     */

    @GetMapping("/listReceivedPacks")
    public TableDataInfo listReceivedPacks(PacksLogs packsLogs) {
        User currentUser = UserUtil.getCurrentUser();
        System.out.println(currentUser);
        startPage();
        packsLogs.setGainPhone(currentUser.getPhone());
        List<PacksLogs> list = packsLogsService.selectPacksLogsList(packsLogs);

        return getDataTable(list);
    }

    /**
     * 导出包裹列表
     */

    @PostMapping("/export")
    public void export(HttpServletResponse response, PacksLogs packsLogs) {
        List<PacksLogs> list = packsLogsService.selectPacksLogsList(packsLogs);
        ExcelUtil<PacksLogs> util = new ExcelUtil<>(PacksLogs.class);
        util.exportExcel(response, list, "包裹数据");
    }

    /**
     * 获取包裹详细信息
     */

    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id) {
        return AjaxResult.success(packsLogsService.selectPacksLogsById(id));
    }

    /**
     * 新增包裹
     */
    @PostMapping
    public AjaxResult add(@RequestBody PacksLogs packsLogs) {
        return toAjax(packsLogsService.insertPacksLogs(packsLogs));
    }

    /**
     * 修改包裹
     */

    @PutMapping
    public AjaxResult edit(@RequestBody PacksLogs packsLogs) {
        return toAjax(packsLogsService.updatePacksLogs(packsLogs));
    }


    /**
     * 删除包裹
     */
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids) {
        return toAjax(packsLogsService.deletePacksLogsByIds(ids));
    }
}
