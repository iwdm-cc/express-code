package com.express.boot.controller;

import com.express.boot.domain.Emps;
import com.express.boot.service.IEmpsService;
import com.express.boot.utils.AjaxResult;
import com.express.boot.utils.BaseController;
import com.express.boot.utils.ExcelUtil;
import com.express.boot.utils.page.TableDataInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;


/**
 * 员工关系Controller
 *
 * @author author__
 * @date 2023-02-03
 */
@RestController
@RequestMapping("/boot/emps")
public class EmpsController extends BaseController {
    @Autowired
    private IEmpsService empsService;

    /**
     * 查询员工关系列表
     */

    @GetMapping("/list")
    public TableDataInfo list(Emps emps) {
        startPage();
        List<Emps> list = empsService.selectEmpsList(emps);
        return getDataTable(list);
    }

    /**
     * 导出员工关系列表
     */

    @PostMapping("/export")
    public void export(HttpServletResponse response, Emps emps) {
        List<Emps> list = empsService.selectEmpsList(emps);
        ExcelUtil<Emps> util = new ExcelUtil<Emps>(Emps.class);
        util.exportExcel(response, list, "员工关系数据");
    }

    /**
     * 获取员工关系详细信息
     */

    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id) {
        return AjaxResult.success(empsService.selectEmpsById(id));
    }

    /**
     * 新增员工关系
     */
    @PostMapping
    public AjaxResult add(@RequestBody Emps emps) {
        return toAjax(empsService.insertEmps(emps));
    }

    /**
     * 修改员工关系
     */

    @PutMapping
    public AjaxResult edit(@RequestBody Emps emps) {
        return toAjax(empsService.updateEmps(emps));
    }

    /**
     * 删除员工关系
     */
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids) {
        return toAjax(empsService.deleteEmpsByIds(ids));
    }
}
