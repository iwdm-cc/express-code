package com.express.boot.controller;

import com.express.boot.domain.SendLogs;
import com.express.boot.domain.User;
import com.express.boot.service.ISendLogsService;
import com.express.boot.utils.AjaxResult;
import com.express.boot.utils.BaseController;
import com.express.boot.utils.ExcelUtil;
import com.express.boot.utils.UserUtil;
import com.express.boot.utils.page.TableDataInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;


/**
 * 派送结果Controller
 *
 * @author author__
 * @date 2023-02-03
 */
@RestController
@RequestMapping("/boot/send")
public class SendLogsController extends BaseController {
    @Autowired
    private ISendLogsService sendLogsService;

    /**
     * 查询派送结果列表
     */

    @GetMapping("/list")
    public TableDataInfo list(SendLogs sendLogs) {
        User currentUser = UserUtil.getCurrentUser();
        System.out.println(currentUser);
        if (!"超级管理员".equals(currentUser.getRoleName())) {
            sendLogs.setEmpId(String.valueOf(currentUser.getId()));
        }

        startPage();
        List<SendLogs> list = sendLogsService.selectSendLogsList(sendLogs);
        return getDataTable(list);
    }

    /**
     * 导出派送结果列表
     */

    @PostMapping("/export")
    public void export(HttpServletResponse response, SendLogs sendLogs) {
        List<SendLogs> list = sendLogsService.selectSendLogsList(sendLogs);
        ExcelUtil<SendLogs> util = new ExcelUtil<SendLogs>(SendLogs.class);
        util.exportExcel(response, list, "派送结果数据");
    }

    /**
     * 获取派送结果详细信息
     */

    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id) {
        return AjaxResult.success(sendLogsService.selectSendLogsById(id));
    }

    /**
     * 新增派送结果
     */
    @PostMapping
    public AjaxResult add(@RequestBody SendLogs sendLogs) {
        return toAjax(sendLogsService.insertSendLogs(sendLogs));
    }

    /**
     * 修改派送结果
     */

    @PutMapping
    public AjaxResult edit(@RequestBody SendLogs sendLogs) {
        return toAjax(sendLogsService.updateSendLogs(sendLogs));
    }

    /**
     * 删除派送结果
     */
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids) {
        return toAjax(sendLogsService.deleteSendLogsByIds(ids));
    }
}
