package com.express.boot.controller;

import com.express.boot.config.SystemConfig;
import com.express.boot.domain.RespBean;
import com.express.boot.utils.BaseController;
import com.express.boot.utils.file.FileUploadUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@RestController
@RequestMapping("/file")
public class FileController  extends BaseController {
    @Autowired
    private SystemConfig systemConfig;
    @PostMapping("/upload")
    public RespBean upload(@RequestParam("file") MultipartFile uploadFile,
                           HttpServletRequest req) {
        String uploadPath = null;
        try {
            String  str = FileUploadUtils.upload(systemConfig.getUploadPathNoend(), uploadFile);

             uploadPath = str.substring(str.indexOf("uploadPath") + "uploadPath".length());
            System.out.println(uploadPath);
        } catch (IOException e) {
            e.getMessage();
        }
        return new RespBean().setMsg(uploadPath);
    }
}
