package com.express.boot.controller;

import com.express.boot.domain.TransitLogs;
import com.express.boot.domain.User;
import com.express.boot.service.ITransitLogsService;
import com.express.boot.utils.AjaxResult;
import com.express.boot.utils.BaseController;
import com.express.boot.utils.ExcelUtil;
import com.express.boot.utils.UserUtil;
import com.express.boot.utils.page.TableDataInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;


/**
 * 物流信息Controller
 *
 * @author author__
 * @date 2023-02-03
 */
@RestController
@RequestMapping("/boot/transit")
public class TransitLogsController extends BaseController {
    @Autowired
    private ITransitLogsService transitLogsService;

    /**
     * 查询物流信息列表
     */

    @GetMapping("/list")
    public TableDataInfo list(TransitLogs transitLogs) {
        startPage();
        List<TransitLogs> list = transitLogsService.selectTransitLogsList(transitLogs);
        return getDataTable(list);
    }

    @GetMapping("/logs")
    public TableDataInfo selectTransitLogsVoByPack(TransitLogs transitLogs) {
        startPage();
        List<TransitLogs> list = transitLogsService.selectTransitLogsVoByPack(transitLogs);
        return getDataTable(list);
    }

    /**
     * 导出物流信息列表
     */

    @PostMapping("/export")
    public void export(HttpServletResponse response, TransitLogs transitLogs) {
        List<TransitLogs> list = transitLogsService.selectTransitLogsList(transitLogs);
        ExcelUtil<TransitLogs> util = new ExcelUtil<TransitLogs>(TransitLogs.class);
        util.exportExcel(response, list, "物流信息数据");
    }

    /**
     * 获取物流信息详细信息
     */

    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id) {
        return AjaxResult.success(transitLogsService.selectTransitLogsById(id));
    }

    /**
     * 新增物流信息
     */
    @PostMapping
    public AjaxResult add(@RequestBody TransitLogs transitLogs) {
        User currentUser = UserUtil.getCurrentUser();
        transitLogs.setEmpId(String.valueOf(currentUser.getId()));
        return toAjax(transitLogsService.insertTransitLogs(transitLogs));
    }

    /**
     * 修改物流信息
     */

    @PutMapping
    public AjaxResult edit(@RequestBody TransitLogs transitLogs) {
        return toAjax(transitLogsService.updateTransitLogs(transitLogs));
    }

    /**
     * 删除物流信息
     */
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids) {
        return toAjax(transitLogsService.deleteTransitLogsByIds(ids));
    }
}
