package com.express.boot.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.express.boot.config.security.SHA256Util;
import com.express.boot.config.security.ShiroUtils;
import com.express.boot.domain.RespBean;
import com.express.boot.domain.User;
import com.express.boot.domain.User_role;
import com.express.boot.mapper.UserMapper;
import com.express.boot.mapper.User_roleMapper;
import com.express.boot.service.UserService;
import com.express.boot.utils.AjaxResult;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.LockedAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

/**
 *
 */
@Slf4j
@RestController
public class LoginController {
    @Resource
    private UserMapper userMapper;

    @Autowired
    private UserService userService;
    @Autowired
    private User_roleMapper user_roleMapper;

    @RequiresRoles("cc")
    @GetMapping("/login")
    public RespBean login() {
        System.out.println(ShiroUtils.getUserInfo().getUsername());
        RespBean respBean = new RespBean();
        return respBean;
    }

    @PostMapping("/login1")
    public Map<String, Object> login(@RequestBody User user) {
        Map<String, Object> map = new HashMap<>();
        //进行身份验证
        try {
            //验证身份和登陆
            Subject subject = SecurityUtils.getSubject();
            UsernamePasswordToken token = new UsernamePasswordToken(user.getUsername(), user.getPassword());
            //进行登录操作
            subject.login(token);
        } catch (IncorrectCredentialsException e) {
            map.put("code", 500);
            map.put("msg", "用户不存在或者密码错误");
            return map;
        } catch (LockedAccountException e) {
            map.put("code", 500);
            map.put("msg", "登录失败，该用户已被冻结");
            return map;
        } catch (AuthenticationException e) {
            map.put("code", 500);
            map.put("msg", "该用户不存在");
            return map;
        } catch (Exception e) {
            map.put("code", 500);
            map.put("msg", "未知异常");
            return map;
        }
        map.put("code", 0);
        map.put("msg", "登录成功");
        map.put("token", ShiroUtils.getSession().getId().toString());
        QueryWrapper<User_role> query = new QueryWrapper<>();
        query.eq("uid", ShiroUtils.getUserInfo().getId());
        User_role user_role = user_roleMapper.selectOne(query);
        map.put("rid", user_role.getRid());
        return map;
    }

    @PostMapping("/register")
    public AjaxResult register(@RequestBody User user) {

        return AjaxResult.success(userService.addUser(user));
    }

    @GetMapping("/updatePwd")
    public AjaxResult updatePwd(User user) {

//        User currentUser = UserUtil.getCurrentUser();
//        String sha256 = SHA256Util.sha256(user.getUsername(), user.getSalt());
//        if (!currentUser.getPassword().equals(sha256)) {
//            return AjaxResult.error("旧密码错误");
//
//        }
        User currentUser = user;
        user.setId(1);
        currentUser.setPassword(SHA256Util.sha256(user.getPassword(), user.getSalt()));
        userMapper.updateById(currentUser);
        return AjaxResult.success("成功");
    }

    @GetMapping("/logout")
    public RespBean logout(User user) {
        ShiroUtils.logout();
        RespBean respBean = new RespBean();
        respBean.setMsg("退出成功");
        return respBean;
    }


}
