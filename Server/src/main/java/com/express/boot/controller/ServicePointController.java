package com.express.boot.controller;

import com.express.boot.domain.ServicePoint;
import com.express.boot.service.IServicePointService;
import com.express.boot.utils.AjaxResult;
import com.express.boot.utils.BaseController;
import com.express.boot.utils.ExcelUtil;
import com.express.boot.utils.page.TableDataInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;



/**
 * 网店管理Controller
 * 
 * @author author__
 * @date 2023-02-03
 */
@RestController
@RequestMapping("/boot/servicepoint")
public class ServicePointController extends BaseController
{
    @Autowired
    private IServicePointService servicePointService;

    /**
     * 查询网店管理列表
     */

    @GetMapping("/list")
    public TableDataInfo list(ServicePoint servicePoint)
    {
        startPage();
        List<ServicePoint> list = servicePointService.selectServicePointList(servicePoint);
        return getDataTable(list);
    }

    /**
     * 导出网店管理列表
     */

    @PostMapping("/export")
    public void export(HttpServletResponse response, ServicePoint servicePoint)
    {
        List<ServicePoint> list = servicePointService.selectServicePointList(servicePoint);
        ExcelUtil<ServicePoint> util = new ExcelUtil<ServicePoint>(ServicePoint.class);
        util.exportExcel(response, list, "网店管理数据");
    }

    /**
     * 获取网店管理详细信息
     */

    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") String id)
    {
        return AjaxResult.success(servicePointService.selectServicePointById(id));
    }

    /**
     * 新增网店管理
     */
    @PostMapping
    public AjaxResult add(@RequestBody ServicePoint servicePoint)
    {
        return toAjax(servicePointService.insertServicePoint(servicePoint));
    }

    /**
     * 修改网店管理
     */

    @PutMapping
    public AjaxResult edit(@RequestBody ServicePoint servicePoint)
    {
        return toAjax(servicePointService.updateServicePoint(servicePoint));
    }

    /**
     * 删除网店管理
     */
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable String[] ids)
    {
        return toAjax(servicePointService.deleteServicePointByIds(ids));
    }
}
