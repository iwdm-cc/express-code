package com.express.boot.utils;


/**
 * @author
 * <p>
 * 操作成功
 * 对象创建成功
 * <p>
 * 请求已经被接受
 * <p>
 * 操作已经执行成功，但是没有返回数据
 * <p>
 * 资源已被移除
 * <p>
 * 重定向
 * <p>
 * 资源没有被修改
 * <p>
 * 参数列表错误（缺少，格式不匹配）
 * <p>
 * 未授权
 * <p>
 * 访问受限，授权过期
 * <p>
 * 资源，服务未找到
 * <p>
 * 不允许的http方法
 * <p>
 * 资源冲突，或者资源被锁
 * <p>
 * 不支持的数据，媒体类型
 * <p>
 * 系统内部错误
 * <p>
 * 操作成功
 * 对象创建成功
 * <p>
 * 请求已经被接受
 * <p>
 * 操作已经执行成功，但是没有返回数据
 * <p>
 * 资源已被移除
 * <p>
 * 重定向
 * <p>
 * 资源没有被修改
 * <p>
 * 参数列表错误（缺少，格式不匹配）
 * <p>
 * 未授权
 * <p>
 * 访问受限，授权过期
 * <p>
 * 资源，服务未找到
 * <p>
 * 不允许的http方法
 * <p>
 * 资源冲突，或者资源被锁
 * <p>
 * 不支持的数据，媒体类型
 * <p>
 * 系统内部错误
 */

/* /*    public static final int SUCCESS = 200;

 *//**
 *操作成功
 * 对象创建成功
 *//*
    public static final int CREATED = 201;

    *//**
 * 请求已经被接受
 *//*
    public static final int ACCEPTED = 202;

    *//**
 * 操作已经执行成功，但是没有返回数据
 *//*
    public static final int NO_CONTENT = 204;

    *//**
 * 资源已被移除
 *//*
    public static final int MOVED_PERM = 301;

    *//**
 * 重定向
 *//*
    public static final int SEE_OTHER = 303;

    *//**
 * 资源没有被修改
 *//*
    public static final int NOT_MODIFIED = 304;

    *//**
 * 参数列表错误（缺少，格式不匹配）
 *//*
    public static final int BAD_REQUEST = 400;

    *//**
 * 未授权
 *//*
    public static final int UNAUTHORIZED = 401;

    *//**
 * 访问受限，授权过期
 *//*
    public static final int FORBIDDEN = 403;

    *//**
 * 资源，服务未找到
 *//*
    public static final int NOT_FOUND = 404;

    *//**
 * 不允许的http方法
 *//*
    public static final int BAD_METHOD = 405;

    *//**
 * 资源冲突，或者资源被锁
 *//*
    public static final int CONFLICT = 409;

    *//**
 * 不支持的数据，媒体类型
 *//*
    public static final int UNSUPPORTED_TYPE = 415;

    *//**
 * 系统内部错误
 *//*
    public static final int ERROR = 500;

    */

/**
 * 接口未实现
 *//*
    public static final int NOT_IMPLEMENTED = 501;*/
public enum CommonEnum implements BaseErrorInfoInterface {
    /*
     * */

    // 数据操作错误定义
    SUCCESS("200", "操作成功!"),
    BODY_NOT_MATCH("400", "请求的数据格式不符!"),
    DataDuplication("801", "数据重复!"),
    SIGNATURE_NOT_MATCH("401", "请求的数字签名不匹配!"),
    NOT_FOUND("404", "未找到该资源!"),
    INTERNAL_SERVER_ERROR("500", "服务器内部错误!"),
    SERVER_BUSY("503", "服务器正忙，请稍后再试!");

    /**
     * 错误码
     */
    private final String resultCode;

    /**
     * 错误描述
     */
    private final String resultMsg;

    CommonEnum(String resultCode, String resultMsg) {
        this.resultCode = resultCode;
        this.resultMsg = resultMsg;
    }

    @Override
    public String getResultCode() {
        return resultCode;
    }

    @Override
    public String getResultMsg() {
        return resultMsg;
    }

}

