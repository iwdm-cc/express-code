package com.express.boot.utils;


import com.express.boot.config.security.ShiroUtils;
import com.express.boot.domain.User;

/**
 * 获取用户信息
 */
public class UserUtil {
    public static User getCurrentUser() {

        User userInfo = ShiroUtils.getUserInfo();
        if (userInfo == null) {
            throw new BizException("请重新登录~","请重新登录~");
        }
        return userInfo;
    }
}
