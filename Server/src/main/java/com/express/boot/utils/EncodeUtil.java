package com.express.boot.utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 52988 on 2018/3/14.
 */
public class EncodeUtil {

    public static String listToStringByDelimiter(List<Integer> list, String delimiter) {

        if (list.size() == 0) return null;

        String ret = list.get(0).toString();

        for (int i = 1; i < list.size(); i++) {
            ret += delimiter + list.get(i).toString();
        }

        return ret;
    }

    public static List<Integer> stringToListByDelimiter(String string, String delimiter) {

        List<Integer> retrunList = new ArrayList<>();

        if (!string.contains(delimiter)) {
            retrunList.add(Integer.valueOf(string));
            return retrunList;
        }

        String[] spString = string.split(delimiter);

        for (int i = 0; i < spString.length; i++) {
            retrunList.add(Integer.valueOf(spString[i]));
        }
        return retrunList;
    }
}
