package com.express.boot.mapper;

import com.express.boot.domain.PacksLogs;

import java.util.List;
import java.util.Set;

/**
 * 包裹Mapper接口
 *
 * @author author__
 * @date 2023-02-03
 */
public interface PacksLogsMapper
{
    /**
     * 查询包裹
     *
     * @param id 包裹主键
     * @return 包裹
     */
    PacksLogs selectPacksLogsById(String id);

    /**
     * 查询包裹列表
     *
     * @param packsLogs 包裹
     * @return 包裹集合
     */
    List<PacksLogs> selectPacksLogsList(PacksLogs packsLogs);

    /**
     * 新增包裹
     *
     * @param packsLogs 包裹
     * @return 结果
     */
    int insertPacksLogs(PacksLogs packsLogs);

    /**
     * 修改包裹
     *
     * @param packsLogs 包裹
     * @return 结果
     */
    int updatePacksLogs(PacksLogs packsLogs);

    /**
     * 删除包裹
     *
     * @param id 包裹主键
     * @return 结果
     */
    int deletePacksLogsById(String id);

    /**
     * 批量删除包裹
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    int deletePacksLogsByIds(String[] ids);

    List<PacksLogs> selectPacksLogsListBySelf(PacksLogs packsLogs);

    List<PacksLogs> selectPacksLogsListByPoints(Set<String> collect);
}
