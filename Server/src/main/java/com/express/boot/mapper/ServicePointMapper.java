package com.express.boot.mapper;

import com.express.boot.domain.ServicePoint;

import java.util.List;

/**
 * 网店管理Mapper接口
 *
 * @author author__
 * @date 2023-02-03
 */
public interface ServicePointMapper {
    /**
     * 查询网店管理
     *
     * @param id 网店管理主键
     * @return 网店管理
     */
    ServicePoint selectServicePointById(String id);

    /**
     * 查询网店管理列表
     *
     * @param servicePoint 网店管理
     * @return 网店管理集合
     */
    List<ServicePoint> selectServicePointList(ServicePoint servicePoint);

    /**
     * 新增网店管理
     *
     * @param servicePoint 网店管理
     * @return 结果
     */
    int insertServicePoint(ServicePoint servicePoint);

    /**
     * 修改网店管理
     *
     * @param servicePoint 网店管理
     * @return 结果
     */
    int updateServicePoint(ServicePoint servicePoint);

    /**
     * 删除网店管理
     *
     * @param id 网店管理主键
     * @return 结果
     */
    int deleteServicePointById(String id);

    /**
     * 批量删除网店管理
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    int deleteServicePointByIds(String[] ids);
}
