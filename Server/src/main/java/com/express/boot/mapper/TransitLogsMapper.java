package com.express.boot.mapper;

import com.express.boot.domain.TransitLogs;

import java.util.List;

/**
 * 物流信息Mapper接口
 *
 * @author author__
 * @date 2023-02-03
 */
public interface TransitLogsMapper {
    /**
     * 查询物流信息
     *
     * @param id 物流信息主键
     * @return 物流信息
     */
    TransitLogs selectTransitLogsById(String id);

    /**
     * 查询物流信息列表
     *
     * @param transitLogs 物流信息
     * @return 物流信息集合
     */
    List<TransitLogs> selectTransitLogsList(TransitLogs transitLogs);
    List<TransitLogs> selectTransitLogsVoByPack(TransitLogs transitLogs);

    /**
     * 新增物流信息
     *
     * @param transitLogs 物流信息
     * @return 结果
     */
    int insertTransitLogs(TransitLogs transitLogs);

    /**
     * 修改物流信息
     *
     * @param transitLogs 物流信息
     * @return 结果
     */
    int updateTransitLogs(TransitLogs transitLogs);

    /**
     * 删除物流信息
     *
     * @param id 物流信息主键
     * @return 结果
     */
    int deleteTransitLogsById(String id);

    /**
     * 批量删除物流信息
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    int deleteTransitLogsByIds(String[] ids);
}
