package com.express.boot.mapper;

import com.express.boot.domain.Emps;

import java.util.List;

/**
 * 员工关系Mapper接口
 * 
 * @author author__
 * @date 2023-02-03
 */
public interface EmpsMapper 
{
    /**
     * 查询员工关系
     * 
     * @param id 员工关系主键
     * @return 员工关系
     */
    public Emps selectEmpsById(String id);

    /**
     * 查询员工关系列表
     * 
     * @param emps 员工关系
     * @return 员工关系集合
     */
    public List<Emps> selectEmpsList(Emps emps);

    /**
     * 新增员工关系
     * 
     * @param emps 员工关系
     * @return 结果
     */
    public int insertEmps(Emps emps);

    /**
     * 修改员工关系
     * 
     * @param emps 员工关系
     * @return 结果
     */
    public int updateEmps(Emps emps);

    /**
     * 删除员工关系
     * 
     * @param id 员工关系主键
     * @return 结果
     */
    public int deleteEmpsById(String id);

    /**
     * 批量删除员工关系
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteEmpsByIds(String[] ids);
}
