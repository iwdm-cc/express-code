package com.express.boot.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.express.boot.domain.User_role;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface User_roleMapper extends BaseMapper<User_role> {
}
