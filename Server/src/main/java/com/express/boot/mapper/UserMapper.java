package com.express.boot.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.express.boot.domain.Role;
import com.express.boot.domain.User;

import java.util.List;


public interface UserMapper extends BaseMapper<User> {
    User loadUserByUsername(String username);

    List<Role> getUserRolesByUid(Integer id);

    List<User> selectUserList(User user);
    int insertUer(User user);
}
