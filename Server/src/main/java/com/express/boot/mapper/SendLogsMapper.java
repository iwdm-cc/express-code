package com.express.boot.mapper;

import com.express.boot.domain.SendLogs;

import java.util.List;

/**
 * 派送结果Mapper接口
 *
 * @author author__
 * @date 2023-02-03
 */
public interface SendLogsMapper
{
    /**
     * 查询派送结果
     *
     * @param id 派送结果主键
     * @return 派送结果
     */
    SendLogs selectSendLogsById(String id);

    /**
     * 查询派送结果列表
     *
     * @param sendLogs 派送结果
     * @return 派送结果集合
     */
    List<SendLogs> selectSendLogsList(SendLogs sendLogs);

    /**
     * 新增派送结果
     *
     * @param sendLogs 派送结果
     * @return 结果
     */
    int insertSendLogs(SendLogs sendLogs);

    /**
     * 修改派送结果
     *
     * @param sendLogs 派送结果
     * @return 结果
     */
    int updateSendLogs(SendLogs sendLogs);

    /**
     * 删除派送结果
     *
     * @param id 派送结果主键
     * @return 结果
     */
    int deleteSendLogsById(String id);

    /**
     * 批量删除派送结果
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    int deleteSendLogsByIds(String[] ids);
}
