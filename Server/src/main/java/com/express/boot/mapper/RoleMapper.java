package com.express.boot.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.express.boot.domain.Role;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface RoleMapper extends BaseMapper<Role> {
    List<Role> getRoleList();
}
