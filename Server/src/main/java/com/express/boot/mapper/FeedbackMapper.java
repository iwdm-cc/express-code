package com.express.boot.mapper;

import com.express.boot.domain.Feedback;

import java.util.List;

/**
 * 留言Mapper接口
 * 
 * @author author__
 * @date 2023-01-27
 */
public interface FeedbackMapper 
{
    /**
     * 查询留言
     * 
     * @param fid 留言主键
     * @return 留言
     */
    public Feedback selectFeedbackByFid(Long fid);

    /**
     * 查询留言列表
     * 
     * @param feedback 留言
     * @return 留言集合
     */
    public List<Feedback> selectFeedbackList(Feedback feedback);

    /**
     * 新增留言
     * 
     * @param feedback 留言
     * @return 结果
     */
    public int insertFeedback(Feedback feedback);

    /**
     * 修改留言
     * 
     * @param feedback 留言
     * @return 结果
     */
    public int updateFeedback(Feedback feedback);

    /**
     * 删除留言
     * 
     * @param fid 留言主键
     * @return 结果
     */
    public int deleteFeedbackByFid(Long fid);

    /**
     * 批量删除留言
     * 
     * @param fids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteFeedbackByFids(Long[] fids);
}
