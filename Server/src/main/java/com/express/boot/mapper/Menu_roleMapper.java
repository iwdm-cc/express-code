package com.express.boot.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.express.boot.domain.Menu_role;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface Menu_roleMapper extends BaseMapper<Menu_role> {
}
