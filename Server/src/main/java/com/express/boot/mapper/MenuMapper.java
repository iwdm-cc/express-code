package com.express.boot.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.express.boot.domain.Menu;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface MenuMapper extends BaseMapper<Menu> {
    List<Menu> getAllMenus();

    List<Menu> menuList();

    List<Menu> allMenuList(String name);

    List<Menu> menuTree();

    List<Menu> menuListBy(int rid);
}
