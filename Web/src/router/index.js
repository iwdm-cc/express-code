import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '../components/Login'
import Home from '../components/Home'
import UserList from '../components/user/userList'
import PowList from '../components/power/PowList'
import RoleList from '../components/power/RoleList'

import Welcome from '../components/Welcome'

Vue.use(VueRouter)

const routes = [{ path: '/', redirect: '/login' }, { path: '/login', component: Login }, {
  path: '/register',
  component: () => import('../components/Register.vue')
}
  , {
    path: '/home', component: Home, redirect: '/welcome', children: [{
      path: '/welcome', component: Welcome
    }, {
      path: '/userList', component: UserList
    }, {
      path: '/powList', component: PowList
    }, {
      path: '/roleList', component: RoleList
    }, {
      path: '/feedback', component: () => import('../views/boot/feedback/index')
    },
       {
      path: '/express/emps', component: () => import('../views/boot/emps/index.vue')
    }, {
      path: '/express/point', component: () => import('../views/boot/servicepoint/index.vue')
    }, {
      path: '/express/packs', component: () => import('../views/boot/packs/index.vue')
    }, {
      path: '/express/allocation', component: () => import('../views/boot/packs/allocation.vue')
    }, {
      path: '/express/send', component: () => import('../views/boot/send/index.vue')
    }, {
      path: '/express/servicepoint', component: () => import('../views/boot/servicepoint/index.vue')
    }, {
      path: '/express/transit', component: () => import('../views/boot/transit/index.vue')
    }, {
      path: '/express/received', component: () => import('../views/boot/packs/received.vue')
    }]
  }]

const router = new VueRouter({
  routes
})
//挂载路由导航守卫
router.beforeEach((to, from, next) => {
  // to将要访问的路径
  // from代表从哪个路径跳转过来
  // next是一个函数表示放行
  // next()放行  next('/login') 跳转到登登录页
  const filterRoutes = ['/login', '/register']
  if (filterRoutes.indexOf(to.path) !== -1) return next()
  //获取用户数据
  const token = window.sessionStorage.getItem('token')

  if (!token) return next('/recipes2')
  next()

})
export default router
