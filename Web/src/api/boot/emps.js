import request from '@/utils/request'

// 查询员工关系列表
export function listEmps(query) {
  return request({
    url: '/boot/emps/list',
    method: 'get',
    params: query
  })
}

// 查询员工关系详细
export function getEmps(id) {
  return request({
    url: '/boot/emps/' + id,
    method: 'get'
  })
}

// 新增员工关系
export function addEmps(data) {
  return request({
    url: '/boot/emps',
    method: 'post',
    data: data
  })
}

// 修改员工关系
export function updateEmps(data) {
  return request({
    url: '/boot/emps',
    method: 'put',
    data: data
  })
}

// 删除员工关系
export function delEmps(id) {
  return request({
    url: '/boot/emps/' + id,
    method: 'delete'
  })
}
