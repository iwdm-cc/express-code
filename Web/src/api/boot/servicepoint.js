import request from '@/utils/request'

// 查询网店管理列表
export function listServicepoint(query) {
  return request({
    url: '/boot/servicepoint/list',
    method: 'get',
    params: query
  })
}

// 查询网店管理详细
export function getServicepoint(id) {
  return request({
    url: '/boot/servicepoint/' + id,
    method: 'get'
  })
}

// 新增网店管理
export function addServicepoint(data) {
  return request({
    url: '/boot/servicepoint',
    method: 'post',
    data: data
  })
}

// 修改网店管理
export function updateServicepoint(data) {
  return request({
    url: '/boot/servicepoint',
    method: 'put',
    data: data
  })
}

// 删除网店管理
export function delServicepoint(id) {
  return request({
    url: '/boot/servicepoint/' + id,
    method: 'delete'
  })
}
