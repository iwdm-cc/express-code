import request from '@/utils/request'

// 查询派送结果列表
export function listSend(query) {
  return request({
    url: '/boot/send/list',
    method: 'get',
    params: query
  })
}

// 查询派送结果详细
export function getSend(id) {
  return request({
    url: '/boot/send/' + id,
    method: 'get'
  })
}

// 新增派送结果
export function addSend(data) {
  return request({
    url: '/boot/send',
    method: 'post',
    data: data
  })
}

// 修改派送结果
export function updateSend(data) {
  return request({
    url: '/boot/send',
    method: 'put',
    data: data
  })
}

// 删除派送结果
export function delSend(id) {
  return request({
    url: '/boot/send/' + id,
    method: 'delete'
  })
}
