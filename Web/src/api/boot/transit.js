import request from '@/utils/request'

// 查询物流信息列表
export function listTransit(query) {
  return request({
    url: '/boot/transit/list',
    method: 'get',
    params: query
  })
}

export function TransitLogs(query) {
  return request({
    url: '/boot/transit/logs',
    method: 'get',
    params: query
  })
}

// 查询物流信息详细
export function getTransit(id) {
  return request({
    url: '/boot/transit/' + id,
    method: 'get'
  })
}

// 新增物流信息
export function addTransit(data) {
  return request({
    url: '/boot/transit',
    method: 'post',
    data: data
  })
}

// 修改物流信息
export function updateTransit(data) {
  return request({
    url: '/boot/transit',
    method: 'put',
    data: data
  })
}

// 删除物流信息
export function delTransit(id) {
  return request({
    url: '/boot/transit/' + id,
    method: 'delete'
  })
}
