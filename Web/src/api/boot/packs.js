import request from '@/utils/request'

// 查询包裹列表
export function listPacks(query) {
  return request({
    url: '/boot/packs/list',
    method: 'get',
    params: query
  })
}
// 网点管理员 获取该网点的订单
export function listAllocationPacks(query) {
  return request({
    url: '/boot/packs/allocation',
    method: 'get',
    params: query
  })
}
// 查询待收货包裹列表
export function listReceivedPacks(query) {
  return request({
    url: '/boot/packs/listReceivedPacks',
    method: 'get',
    params: query
  })
}

// 查询包裹详细
export function getPacks(id) {
  return request({
    url: '/boot/packs/' + id,
    method: 'get'
  })
}

// 新增包裹
export function addPacks(data) {
  return request({
    url: '/boot/packs',
    method: 'post',
    data: data
  })
}

// 修改包裹
export function updatePacks(data) {
  return request({
    url: '/boot/packs',
    method: 'put',
    data: data
  })
}

// 删除包裹
export function delPacks(id) {
  return request({
    url: '/boot/packs/' + id,
    method: 'delete'
  })
}
