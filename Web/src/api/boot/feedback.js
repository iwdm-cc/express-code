import request from '@/utils/request'

// 查询留言列表
export function listFeedback(query) {
  return request({
    url: '/boot/feedback/list',
    method: 'get',
    params: query
  })
}

// 查询留言详细
export function getFeedback(fid) {
  return request({
    url: '/boot/feedback/' + fid,
    method: 'get'
  })
}

// 新增留言
export function addFeedback(data) {
  return request({
    url: '/boot/feedback',
    method: 'post',
    data: data
  })
}

// 修改留言
export function updateFeedback(data) {
  return request({
    url: '/boot/feedback',
    method: 'put',
    data: data
  })
}

// 删除留言
export function delFeedback(fid) {
  return request({
    url: '/boot/feedback/' + fid,
    method: 'delete'
  })
}
